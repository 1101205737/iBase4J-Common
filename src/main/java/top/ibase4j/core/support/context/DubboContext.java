package top.ibase4j.core.support.context;

import com.alibaba.dubbo.config.ApplicationConfig;
import com.alibaba.dubbo.config.ReferenceConfig;
import com.alibaba.dubbo.config.RegistryConfig;

@SuppressWarnings("unchecked")
public class DubboContext {
    private static ApplicationConfig dubboApplication;
    private static RegistryConfig dubboRegistry;

    public static <T> T getService(Class<T> cls) {
        if (dubboApplication == null || dubboRegistry == null) {
            synchronized (DubboContext.class) {
                if (dubboApplication == null) {
                    dubboApplication = ApplicationContextHolder.getBean(ApplicationConfig.class);
                }
                if (dubboRegistry == null) {
                    dubboRegistry = ApplicationContextHolder.getBean(RegistryConfig.class);
                }
            }
        }
        // 引用远程服务
        ReferenceConfig<?> reference = new ReferenceConfig<>();
        reference.setApplication(dubboApplication);
        reference.setRegistry(dubboRegistry); // 多个注册中心可以用setRegistries()
        reference.setCheck(false);
        reference.setInterface(cls);
        return (T)reference.get();
    }
}
