package top.ibase4j.core.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

import com.alibaba.dubbo.config.ConsumerConfig;
import com.alibaba.dubbo.config.ProtocolConfig;
import com.alibaba.dubbo.config.ProviderConfig;
import com.alibaba.dubbo.config.spring.context.annotation.DubboComponentScan;

import top.ibase4j.core.support.rpc.EnableDubboReference;
import top.ibase4j.core.support.rpc.EnableDubboService;
import top.ibase4j.core.support.rpc.EnableMotan;
import top.ibase4j.core.util.PropertiesUtil;

/**
 * RPC服务配置
 * @author ShenHuaJie
 * @since 2017年8月14日 上午10:16:18
 */
public class RpcConfig {
    @Configuration
    @Conditional(EnableDubboService.class)
    @DubboComponentScan("${rpc.package}")
    static class DubboServiceConfig extends DubboBaseConfig {
        @Bean
        public ProviderConfig provider() {
            ProviderConfig providerConfig = new ProviderConfig();
            providerConfig.setFilter("dataSourceAspect,default");
            providerConfig.setTimeout(PropertiesUtil.getInt("rpc.connect.timeout", 20000));
            return providerConfig;
        }

        @Bean
        public ProtocolConfig protocol() {
            ProtocolConfig protocolConfig = new ProtocolConfig();
            protocolConfig.setHost(PropertiesUtil.getString("rpc.protocol.post"));
            protocolConfig.setPort(PropertiesUtil.getInt("rpc.protocol.port", 20880));
            protocolConfig.setThreadpool("cached");
            protocolConfig.setThreads(PropertiesUtil.getInt("rpc.protocol.maxThread", 100));
            protocolConfig.setPayload(PropertiesUtil.getInt("rpc.protocol.maxContentLength", 1048576));
            return protocolConfig;
        }
    }

    @Configuration
    @Conditional(EnableDubboReference.class)
    static class DubboConsumerConfig extends DubboBaseConfig {
        @Bean
        public ConsumerConfig consumer() {
            ConsumerConfig consumerConfig = new ConsumerConfig();
            consumerConfig.setLoadbalance("leastactive");
            consumerConfig.setTimeout(PropertiesUtil.getInt("rpc.request.timeout", 20000));
            consumerConfig.setRetries(PropertiesUtil.getInt("rpc.consumer.retries", 0));
            consumerConfig.setCheck(false);
            return consumerConfig;
        }
    }

    @Configuration
    @Conditional(EnableMotan.class)
    @ImportResource({"classpath*:spring/motan.xml"})
    static class MotanConfig {
    }
}
